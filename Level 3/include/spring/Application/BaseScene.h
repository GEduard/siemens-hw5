#pragma once

#include <qlabel.h>
#include <qobject.h>
#include <qwidget.h>
#include <qpushbutton.h>
#include <spring\Framework\IScene.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
			private slots:
				void nextPushed();
				void previousPushed();

		private:
			QLabel *sceneIndex;
			QWidget *centralWidget;
			QPushButton *nextButton;
			QPushButton *previousButton;

		public:
			explicit BaseScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}