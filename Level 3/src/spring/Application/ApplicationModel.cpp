#include <spring\Application\BaseScene.h>
#include <spring\Application\ApplicationModel.h>

const std::string& firstSceneName = "Scene1";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{

	}

	void ApplicationModel::defineScene()
	{
		std::string sceneName;
		for (int i = 1; i <= 1000; i++)
		{
			sceneName = "Scene";
			sceneName.append(std::to_string(i));
			IScene* newScene = new BaseScene(sceneName);
			m_Scenes.emplace(sceneName, newScene);
		}
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = firstSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		int defaultValue = 1;
		m_TransientData.emplace("sceneIndex", defaultValue);
	}
}
