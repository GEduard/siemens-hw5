#include <spring\Application\BaseScene.h>
#include <iostream>

namespace Spring
{
	BaseScene::BaseScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void BaseScene::createScene()
	{
		createGUI();
		connect(nextButton, SIGNAL(clicked()), this, SLOT(nextPushed()));
		connect(previousButton, SIGNAL(clicked()), this, SLOT(previousPushed()));
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}

	void BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		sceneIndex = new QLabel(centralWidget);
		sceneIndex->setObjectName(QStringLiteral("sceneIndex"));
		sceneIndex->setGeometry(QRect(170, 70, 91, 31));
		previousButton = new QPushButton(centralWidget);
		previousButton->setObjectName(QStringLiteral("previousBUtton"));
		previousButton->setGeometry(QRect(60, 170, 75, 23));
		nextButton = new QPushButton(centralWidget);
		nextButton->setObjectName(QStringLiteral("nextButton"));
		nextButton->setGeometry(QRect(250, 170, 75, 23));
		m_uMainWindow.get()->setCentralWidget(centralWidget);
		previousButton->setText("Previous");
		nextButton->setText("Next");
		std::string currentIndex = std::to_string(boost::any_cast<int>(m_TransientDataCollection.find("sceneIndex")->second));
		sceneIndex->setText(QString::fromStdString(currentIndex));
	}
	
	void BaseScene::nextPushed()
	{
		int currentIndex = std::stoi(sceneIndex->text().toStdString());
		if (currentIndex < 1000)
		{
			std::string nextSceneName = "Scene";
			nextSceneName.append(std::to_string(currentIndex + 1));
			m_TransientDataCollection.erase("sceneIndex");
			m_TransientDataCollection.emplace("sceneIndex", currentIndex + 1);
			emit SceneChange(nextSceneName);
		}
	}

	void BaseScene::previousPushed()
	{
		int currentIndex = std::stoi(sceneIndex->text().toStdString());
		if (currentIndex > 1)
		{
			std::string prevSceneName = "Scene";
			prevSceneName.append(std::to_string(currentIndex - 1));
			m_TransientDataCollection.erase("sceneIndex");
			m_TransientDataCollection.emplace("sceneIndex", currentIndex - 1);
			emit SceneChange(prevSceneName);
		}
	}
}
