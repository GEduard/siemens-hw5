#include <spring\Application\FirstScene.h>
#include <spring\Application\ApplicationModel.h>

const std::string& firstSceneName = "FirstScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{

	}

	void ApplicationModel::defineScene()
	{
		IScene* firstScene = new FirstScene(firstSceneName);
		m_Scenes.emplace(firstSceneName, firstScene);
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = firstSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		std::string labelText = "Hello World";
		m_TransientData.emplace("label", labelText);
	}
}
