#include <spring\Application\FirstScene.h>

namespace Spring
{
	FirstScene::FirstScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void FirstScene::createScene()
	{
		createGUI();
	}

	void FirstScene::release()
	{
		delete centralWidget;
	}

	void FirstScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));
		label->setGeometry(QRect(150, 120, 61, 31));
		m_uMainWindow.get()->setCentralWidget(centralWidget);
		mainToolBar = new QToolBar(m_uMainWindow.get());
		m_uMainWindow.get()->setObjectName(QStringLiteral("mainToolBar"));
		m_uMainWindow.get()->addToolBar(Qt::TopToolBarArea, mainToolBar);
		statusBar = new QStatusBar(m_uMainWindow.get());
		m_uMainWindow.get()->setObjectName(QStringLiteral("statusBar"));
		m_uMainWindow.get()->setStatusBar(statusBar);
		label->setText("Hello World");
	}
}