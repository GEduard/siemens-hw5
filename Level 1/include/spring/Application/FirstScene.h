#pragma once

#include <qlabel.h>
#include <qtoolbar.h>
#include <qstatusbar.h>
#include <spring\Framework\IScene.h>

namespace Spring
{
	class FirstScene : public IScene
	{
		private:
			QWidget *centralWidget;
			QLabel *label;
			QToolBar *mainToolBar;
			QStatusBar *statusBar;

		public:
			explicit FirstScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}
