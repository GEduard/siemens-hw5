#include <qapplication.h>
#include <spring\Framework\Application.h>
#include <spring\Application\ApplicationModel.h>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	Spring::Application &application = Spring::Application::getInstance();
	Spring::ApplicationModel applicationModel;
	application.setApplicationModel(&applicationModel);
	application.start("Level 2", 420, 420);
	app.exec();
	return 0;
}