#include <spring\Application\FirstScene.h>
#include <spring\Application\SecondScene.h>

namespace Spring
{
	SecondScene::SecondScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void SecondScene::createScene()
	{
		createGUI();
		connect(backButton, SIGNAL(clicked()), this, SLOT(backPushed()));
	}

	void SecondScene::release()
	{
		delete centralWidget;
	}

	void SecondScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		m_uMainWindow.get()->setCentralWidget(centralWidget);
		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("backButton"));
		backButton->setGeometry(QRect(150, 180, 75, 25));
		backButton->setText("Go Back!");
		messageLabel = new QLabel(centralWidget);
		messageLabel->setObjectName(QStringLiteral("messageLabel"));
		messageLabel->setGeometry(QRect(125, 80, 120, 30));
		std::string input = boost::any_cast<std::string>(m_TransientDataCollection.find("inputBox")->second);
		messageLabel->setText("Hello " + QString::fromStdString(input));
	}

	void SecondScene::backPushed()
	{
		emit SceneChange("FirstScene");
	}
}
