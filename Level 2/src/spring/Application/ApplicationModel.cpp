#include <spring\Application\FirstScene.h>
#include <spring\Application\SecondScene.h>
#include <spring\Application\ApplicationModel.h>

const std::string& firstSceneName = "FirstScene";
const std::string& secondSceneName = "SecondScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{

	}

	void ApplicationModel::defineScene()
	{
		IScene* firstScene = new FirstScene(firstSceneName);
		IScene* secondScene = new SecondScene(secondSceneName);
		m_Scenes.emplace(firstSceneName, firstScene);
		m_Scenes.emplace(secondSceneName, secondScene);
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = firstSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		std::string defaultText = "";
		m_TransientData.emplace("inputBox", defaultText);
	}
}
