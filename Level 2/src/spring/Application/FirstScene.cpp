#include <spring\Application\FirstScene.h>

namespace Spring
{
	FirstScene::FirstScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void FirstScene::createScene()
	{
		createGUI();
		connect(helloButton, SIGNAL(clicked()), this, SLOT(helloPushed()));
	}

	void FirstScene::release()
	{
		delete centralWidget;
	}

	void FirstScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		m_uMainWindow.get()->setCentralWidget(centralWidget);
		centralWidgetLayout = new QGridLayout(centralWidget);
		centralWidgetLayout->setSpacing(5);
		centralWidgetLayout->setContentsMargins(11, 11, 11, 11);
		centralWidgetLayout->setObjectName(QStringLiteral("centralWidgetLayout"));
		inputBox = new QLineEdit(centralWidget);
		inputBox->setObjectName(QStringLiteral("lineEdit"));
		inputBox->setGeometry(QRect(90, 80, 190, 20));
		std::string input = boost::any_cast<std::string>(m_TransientDataCollection.find("inputBox")->second);
		inputBox->setText(QString::fromStdString(input));
		helloButton = new QPushButton(centralWidget);
		helloButton->setObjectName(QStringLiteral("helloButton"));
		helloButton->setGeometry(QRect(150, 140, 75, 25));
		helloButton->setText("Say Hello!");
	}
	
	void FirstScene::helloPushed()
	{
		if (inputBox->text() != "")
		{
			QString input = inputBox->text();
			m_TransientDataCollection.erase("inputBox");
			m_TransientDataCollection.emplace("inputBox", input.toStdString());
			emit SceneChange("SecondScene");
		}
	}
}
