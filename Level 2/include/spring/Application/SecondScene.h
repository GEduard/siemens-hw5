#pragma once

#include <qlabel.h>
#include <qwidget.h>
#include <qobject.h>
#include <qpushbutton.h>
#include <spring\Framework\IScene.h>

namespace Spring
{
	class SecondScene : public IScene
	{
		Q_OBJECT
			private slots:
				void backPushed();

		private:
			QLabel *messageLabel;
			QWidget *centralWidget;
			QPushButton *backButton;

		public:
			explicit SecondScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}