#pragma once

#include <qlabel.h>
#include <qobject.h>
#include <qwidget.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qgridlayout.h>
#include <spring\Framework\IScene.h>

namespace Spring
{
	class FirstScene : public IScene
	{
		Q_OBJECT
			private slots:
				void helloPushed();

		private:
			QLineEdit *inputBox;
			QWidget *centralWidget;
			QPushButton *helloButton;
			QGridLayout *centralWidgetLayout;

		public:
			explicit FirstScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}